-- chikun :: 2015
-- Cursors


cursor = {
    razor       = m.newCursor("cur/razor.png", 14, 9),
    butter      = m.newCursor("cur/butter.png", 10, 10),
    pizza       = m.newCursor("cur/pizza.png", 4, 4),
    tattoo      = m.newCursor("cur/tattoo.png", 2, 2),
    lipstick    = m.newCursor("gfx/beautifulLipstickBrush.png", 31, 4),
    mascara     = m.newCursor("gfx/beautifulMascaraBrush.png", 32, 12),
    eyeliner    = m.newCursor("gfx/beautifulEyelinerBrush.png", 29, 12),
    hand        = m.newCursor("cur/hand.png", 3, 1)
}
