-- chikun :: 2015
-- Ludum Dare 32



--[[
    Here we will load the chikun specific libaries
  ]]
require "lib/bindings"      -- Very important that you read this file

-- Load particle functions
require "lib/iGrid"

-- Set default scaling
g.setDefaultFilter('linear')

require "lib/maths"         -- Load maths functions
require "lib/misc"          -- Load miscellaneous functions

--[[
    The recursive loading functions pretty much convert / to . and
    make file extensions void.
    eg. gfx/player.png becomes gfx.player,
        sfx/enemies/die becomes sfx.enemies.die
  ]]
require "lib/load/fonts"    -- Load fonts into fnt table [not recursive]
require "lib/load/gfx"      -- Load png graphics from gfx folder to gfx table
require "lib/load/maps"     -- Load all maps from map folder to maps table
require "lib/load/snd"      -- Load ogg sounds from bgm and sfx to bgm and sfx
require "lib/load/states"   -- Load states from src/states to states table

require "lib/stateManager"  -- Load state manager
require "lib/mapTool"       -- Load mapTool :: don't try reading this

require "src/player"        -- Load the player functions




--[[

Use AirDroid

]]




-- Performed at game startup
function love.load()

    -- Set game width and height
    gameWidth, gameHeight = 1024,
        (love.window.getHeight() / love.window.getWidth()) * 1024

    -- Set current state to play state
    state.load(states.splash)

    -- How fast the game progresses
    gameSpeed = 1

    -- In-game touch
    touch = {
        x = 0,
        y = 0,
        w = 1,
        h = 1
    }


end



-- Performed on game update
function love.update(dt)


    --[[
        Limit dt to 1/15 and induce lag if below 15 FPS.
        This is to save calculations.
      ]]
    dt = math.min(dt, 1/15) * gameSpeed



    -- Update touch
    touch.x = love.mouse.getX() / love.window.getWidth() * gameWidth
    touch.y = love.mouse.getY() / love.window.getWidth() * gameWidth


    -- Update current state
    state.current:update(dt)
end



-- Performed on game draw
function love.draw()


    -- Set drawing colour to white [default]
    g.setColor(255, 255, 255)


    -- Scale window size
    love.graphics.scale(love.window.getWidth() / 1024)


    -- Draw current state
    state.current:draw()


    -- Reset game scaling
    love.graphics.origin()


end


-- On state resize
function love.resize(w, h)

    -- Resize window to scale
    gameWidth, gameHeight = 1024, (h / w) * 1024

end
