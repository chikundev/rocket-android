-- chikun :: 2015
-- The configuration file for our tutorial


--[[
    The function which runs at the start of the game, and
    allows us to modify the game window
  ]]
function love.conf(game)

    game.window.width   = 360
    game.window.height  = 640
    game.window.title   = "Android Rocket Game"
    game.window.vsync   = false

    game.window.resizable = true
    --[[
        On Windows, will attach a debug console to the game.
        Set to false before distribution.
      ]]
    game.console = false

end
