-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
player = {

}


-- On player create
function player:create(x, y)

    -- Set player size
    player.w    = 112
    player.h    = 500

    -- Set player image
    player.image    = gfx.rocket

    -- Set image offsets
    player.xOffset  = 66
    player.yOffset  = 243

    -- Set player positions
    player.x = x - (player.image:getWidth() / 2) + player.xOffset
    player.y = gameHeight - (player.h )

    -- Set staring rotation
    player.rotation = 0

    -- Set starting speed
    player.shipSpeed = 1

    -- Time to speed up ship
    time = 0

end


-- On player update
function player:update(dt)

    -- Stop rocket from twitching
    if math.abs(touch.x - player.x - player.xOffset) < 4 then
        player.x = touch.x - player.xOffset
    end

    -- Find correct touch location
    local xMove = math.sign(touch.x - player.x - player.xOffset)

    -- Ship Speed increases every 10 seconds
    if time + 10 < love.timer.getTime() then
        player.shipSpeed = player.shipSpeed + 0.1
        time = love.timer.getTime()
    end


    -- Move the player
    player.x = (player.x + (xMove * 360 * dt * player.shipSpeed))


    -- Rotations
    player.rotation = player.rotation + dt

end


-- On player draw
function player:draw()

    -- Draw player
    g.draw(player.image, player.x + player.xOffset,
        player.y + player.yOffset, 0, 1, 1,
        player.image:getWidth() / 2, player.yOffset)

end


-- On player kill
function player:kill()

end
