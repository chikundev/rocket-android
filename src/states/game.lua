-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local game = { }


-- On state create
function game:create()

    -- Create player
    player:create(512, 400)

    -- Set bgm
    bgm.mystery:play()
end


-- On state update
function game:update(dt)

    -- Update speed of scrolling
    bgScroll = (bgScroll + dt * 512) % gfx.sky:getHeight()

    player:update(dt)

    if love.keyboard.isDown("escape") then
        bgm.mystery:pause()
        love.load(states.menu)
    end
end


-- On state draw
function game:draw()

    g.setColor(255, 255, 255)

    -- Draw the bg image three times connected to eachother
    g.draw(gfx.sky, 0, bgScroll - gfx.sky:getHeight())
    g.draw(gfx.sky, 0, bgScroll)
    g.draw(gfx.sky, 0, bgScroll + gfx.sky:getHeight())

    player:draw()

end


-- On state kill
function game:kill()

end


-- Transfer data to state loading script
return game
