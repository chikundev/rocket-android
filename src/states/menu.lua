-- chikun :: 2014
-- Menu state


-- Temporary state, removed at end of script
local menuState = { }


-- On state create
function menuState:create()

    menuVars = {
        selected  = 1,
        timer     = 0,
    }

    -- Set scrolling
    bgScroll = 0

    width = love.window.getWidth()
    height = love.window.getHeight()

end


-- On state update
function menuState:update(dt)

    -- Update speed of scrolling
    bgScroll = (bgScroll + dt * 512) % gfx.sky:getHeight()

    -- Preven speed scrolling in menu
    if menuVars.timer == 0 then

        -- Check if we are trying to scroll through menu
        if love.keyboard.isDown("down", "up", "s", "w") then
            local dir = 0

            -- Check if scrolling down
            if love.keyboard.isDown("down", "s") then
                dir = dir + 1
            end

            -- Check if scrolling up
            if love.keyboard.isDown("up", "w") then
                dir = dir - 1
            end

            -- Scroll in appropriate direction
            menuVars.selected = menuVars.selected + dir

            -- Send scroll to start if run out
            if menuVars.selected >= 4 then
                menuVars.selected = 1
            end

            -- Send scroll to end if run out
            if menuVars.selected <= 0 then
                menuVars.selected = 3
            end

            -- Set timer to prevent speed Scrolling
            if dir ~= 0 then
                menuVars.timer = 0.2
            end
        end
    else

        -- Reset timer to allow button mash scrolling
        if not love.keyboard.isDown("down", "up", "s", "w") then
            menuVars.timer = 0
        end

        -- Coun down timer
        menuVars.timer = menuVars.timer - dt

        -- Prevent timer becoming less than zero
        if menuVars.timer <= 0 then
            menuVars.timer = 0
        end
    end

    -- Do things upon action for different menus
    if love.keyboard.isDown(" ", "return") then

        -- Will become option to go to play state
        if menuVars.selected == 1 then

            state.load(states.game)

        -- Will become option to go to options menu
        elseif menuVars.selected == 2 then

            -- todo: Make options menu

        -- Leaves game if want to quit
        elseif menuVars.selected == 3 then

            e.quit()

        end

    end

    local width = love.window.getWidth()
    local height = love.window.getHeight()

    -- Mouse in box sets state to game
    a, b = love.mouse.getPosition()
    if a > width/20 and a < (width/20)*19 and b > height/10 and b < (height/4)+height/20 then
        state.load(states.game)
    end

end


-- On state draw
function menuState:draw()

    -- Set font
    g.setFont(fnt.splash)

    -- Draw bg
    g.setColor(255, 255, 255)

    -- Draw the bg image three times connected to eachother
    g.draw(gfx.sky, 0, bgScroll - gfx.sky:getHeight())
    g.draw(gfx.sky, 0, bgScroll)
    g.draw(gfx.sky, 0, bgScroll + gfx.sky:getHeight())

    -- Draw bg
    g.setColor(0, 0, 0)

    -- Drawing three menu options
    g.print("Play", 26, 50, 0, 2, 2)
    g.print("Options", 26, 550)
    g.print("Exit", 26, 1050)

end


-- On state kill
function menuState:kill()

    -- Kill menuVars
    menuVars = nil

end


-- Transfer data to state loading script
return menuState
